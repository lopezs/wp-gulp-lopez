<?php
/**
 * Child theme functions and definitions.
 *
 * Add your custom PHP in this file. 
 * Only edit this file if you have direct access to it on your server (to fix errors if they happen).
 *
 * Note: If using GeneratePress Parent theme you do not need to include wp_enqueue_style the parent as GP already handles this
 *
 */

// Prevent GP from loading child css
add_action( 'wp_enqueue_scripts', function() {
    wp_dequeue_style( 'generate-child' );
}, 50 );

function generatepress_child_enqueue_scripts() {

    wp_enqueue_style('child-css',
           get_stylesheet_directory_uri() . '/style.min.css', array(),
           filemtime(get_stylesheet_directory() . '/style.css'), false);

     if (file_exists(get_stylesheet_directory() . '/assets/js/vendor.js')) {
        wp_enqueue_script('vendor',
            get_stylesheet_directory_uri() . '/assets/js/vendor.js?'
            . filemtime(get_stylesheet_directory() . '/assets/js/vendor.js'),
            [], null, true
        );
    }
    if (file_exists(get_stylesheet_directory() . '/assets/js/custom.min.js')) {
        wp_enqueue_script(
            'custom',
            get_stylesheet_directory_uri() . '/assets/js/custom.min.js?'
            . filemtime(
                get_stylesheet_directory() . '/assets/js/custom.min.js'
            ), [], null, true
        );
    }
}
add_action( 'wp_enqueue_scripts', 'generatepress_child_enqueue_scripts', 100 );

