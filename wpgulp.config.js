/**
 * WPGulp Configuration File
 *
 * 1. Edit the variables as per your project requirements.
 * 2. In paths you can add <<glob or array of globs>>.
 *
 * @package WPGulp
 */

// Project options.
const projectURL = 'wp-gulp-lopez.ddev.site'; // Local project URL of your already running WordPress site. Could be something like wpgulp.local or localhost:3000 depending upon your local WordPress setup.
const projectName = 'wp-gulp-lopez'; // Set the project name to be used in zipping up the production project
const productURL = './'; // Theme/Plugin URL. Leave it like it is, since our gulpfile.js lives in the root folder.
const browserAutoOpen = false;
const injectChanges = true;

// Style options.
const styleSRC = './assets/scss/style.scss'; // Path to main .scss file.
const styleDestination = './'; // Path to place the compiled CSS file. Default set to root folder.
const outputStyle = 'expanded'; // Available options → 'compact' or 'compressed' or 'nested' or 'expanded'
const errLogToConsole = true;
const precision = 10;

// JS Vendor options.
const jsVendorSRC = './assets/js/vendor/*.js'; // Path to JS vendor folder.
const jsVendorDestination = './assets/js/'; // Path to place the compiled JS vendors file.
const jsVendorFile = 'vendor'; // Compiled JS vendors file name. Default set to vendors i.e. vendors.js.

// JS Custom options.
const jsCustomSRC = './assets/js/custom/*.js'; // Path to JS custom scripts folder.
const jsCustomDestination = './assets/js/'; // Path to place the compiled JS custom scripts file.
const jsCustomFile = 'custom'; // Compiled JS custom file name. Default set to custom i.e. custom.js.

// Images options.
const imgSRC = './assets/images/raw/**/*'; // Source folder of images which should be optimized and watched. You can also specify types e.g. raw/**.{png,jpg,gif} in the glob.
const imgDST = './assets/images/'; // Destination folder of optimized images. Must be different from the imagesSRC folder.

// Watch files paths.
const watchStyles = './assets/scss/**/*.scss'; // Path to all *.scss files inside css folder and inside them.
const watchJsVendor = './assets/js/vendor/*.js'; // Path to all vendor JS files.
const watchJsCustom = './assets/js/custom/*.js'; // Path to all custom JS files.
const watchPhp = './**/*.php'; // Path to all PHP files.

// Translation options.
const textDomain = 'lopezs'; // Your textdomain here.
const translationFile = 'lopezs.pot'; // Name of the translation file.
const translationDestination = './languages'; // Where to save the translation files.
const packageName = 'lopezs'; // Package name.

// Zip file config.
// Must have.zip at the end.
const zipName = '${projectName}.zip';

// Must be a folder outside of the zip folder.
const zipDestination = './build/'; // Default: Parent folder.
const zipIncludeGlob = ['./**/*']; // Default: Include all files/folders in current directory.

// Default ignored files and folders for the zip file.
const zipIgnoreGlob = [
  '!./{node_modules,node_modules/**/*}',
  '!./{build,build/**/*}',
	'!./.git',
	'!./.svn',
	'!./gulpfile.babel.js',
	'!./wpgulp.config.js',
	'!./.eslintrc.js',
	'!./.eslintignore',
	'!./.editorconfig',
	'!./phpcs.xml.dist',
  '!./vscode',
	'!./package.json',
	'!./package-lock.json',
	'!./assets/scss/**/*',
	'!./assets/scss',
	'!./assets/images/raw/**/*',
  '!./assets/images/raw',
  '!./*.map',
	`!${imgSRC}`,
	`!${styleSRC}`,
	`!${jsCustomSRC}`,
	`!${jsVendorSRC}`
];


	// Browsers you care about for autoprefixing. Browserlist https://github.com/ai/browserslist
 const BROWSERS_LIST =[
		'last 2 version',
		'> 1%',
	];


module.exports = {
  projectURL,
  projectName,
  productURL,
  browserAutoOpen,
  injectChanges,
  styleSRC,
  styleDestination,
  outputStyle,
  errLogToConsole,
  precision,
  jsVendorSRC,
  jsVendorDestination,
  jsVendorFile,
  jsCustomSRC,
  jsCustomDestination,
  jsCustomFile,
  imgSRC,
  imgDST,
  watchStyles,
  watchJsVendor,
  watchJsCustom,
  watchPhp,
  textDomain,
  translationFile,
  translationDestination,
  zipName,
  zipDestination,
  zipIncludeGlob,
  zipIgnoreGlob,
  packageName,
  BROWSERS_LIST
};
