#

## Clone project to theme folder

```sh
git clone git@gitlab.com:lopezs/wp-gulp-lopez.git wp-theme-folder
cd wp-theme-folder
npm install
```

Edit `wpgulp.config.js` with suitable values for the project

## Tasks

```sh
# Start monitoring assets, detect changes and compile as necessary
npm start

# Build a zip package suitable for deployment
gulp deploy

```
